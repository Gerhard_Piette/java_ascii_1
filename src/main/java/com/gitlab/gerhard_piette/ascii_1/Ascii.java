package com.gitlab.gerhard_piette.ascii_1;

public class Ascii {

	public static String stringOf(int cp){
		var ret = new StringBuilder();
		ret.appendCodePoint(cp);
		return ret.toString();
	}

	/**
	 * A string with escape sequences.
	 * https://docs.oracle.com/javase/tutorial/java/data/characters.html
	 *
	 * @param cp
	 * @return
	 */
	public static String stringWithEscapeOf(int cp){
		var ret = new StringBuilder();
		switch(cp) {
			case '\t' : ret.append("\\t");break;
			case '\b' : ret.append("\\b");break;
			case '\n' : ret.append("\\n");break;
			case '\r' : ret.append("\\r");break;
			case '\f' : ret.append("\\f");break;
			case '\'' : ret.append("\\'");break;
			case '\"' : ret.append("\\\"");break;
			case '\\' : ret.append("\\\\");break;
			default: ret.appendCodePoint(cp);
		}
		return ret.toString();
	}

	public static final int Null = 0;
	public static final int Start_of_Header = 1;
	public static final int Start_of_Text = 2;
	public static final int End_of_Text = 3;
	public static final int End_of_Transmission = 4;
	public static final int Enquiry = 5;
	public static final int Acknowledgment = 6;
	public static final int Bell = 7;
	public static final int Backspace = 8;
	public static final int Horizontal_Tab = 9;
	public static final int Line_feed = 10;
	public static final int Vertical_Tab = 11;
	public static final int Form_feed = 12;
	public static final int Carriage_return = 13;
	public static final int Shift_Out = 14;
	public static final int Shift_In = 15;
	public static final int Data_Link_Escape = 16;
	public static final int DC1 = 17;
	public static final int DC2 = 18;
	public static final int DC3 = 19;
	public static final int DC4 = 20;
	public static final int Negative_Acknowledge = 21;
	public static final int Synchronous_idle = 22;
	public static final int End_of_Transmission_Block = 23;
	public static final int Cancel = 24;
	public static final int End_of_Medium = 25;
	public static final int Substitute = 26;
	public static final int Escape = 27;
	public static final int File_Separator = 28;
	public static final int Group_Separator = 29;
	public static final int Record_Separator = 30;
	public static final int Unit_Separator = 31;
	public static final int Space = 32;
	public static final int Exclamation_Mark = 33;
	public static final int Quotation_Mark = 34;
	public static final int Number_Sign = 35;
	public static final int Dollar_Sign = 36;
	public static final int Percent_Sign = 37;
	public static final int Ampersand = 38;
	public static final int Apostrophe = 39;
	public static final int Left_Parenthesis = 40;
	public static final int Right_Parenthesis = 41;
	public static final int Asterisk = 42;
	public static final int Plus = 43;
	public static final int Comma = 44;
	public static final int Hyphen_Minus = 45;
	public static final int Full_stop = 46;
	public static final int Solidus = 47;
	public static final int Digit_0 = 48;
	public static final int Digit_1 = 49;
	public static final int Digit_2 = 50;
	public static final int Digit_3 = 51;
	public static final int Digit_4 = 52;
	public static final int Digit_5 = 53;
	public static final int Digit_6 = 54;
	public static final int Digit_7 = 55;
	public static final int Digit_8 = 56;
	public static final int Digit_9 = 57;
	public static final int Colon = 58;
	public static final int Semicolon = 59;
	public static final int Less_Than = 60;
	public static final int Equals_Sign = 61;
	public static final int Greater_Than = 62;
	public static final int Question_Mark = 63;
	public static final int Commercial_At = 64;
	public static final int Capital_A = 65;
	public static final int Capital_B = 66;
	public static final int Capital_C = 67;
	public static final int Capital_D = 68;
	public static final int Capital_E = 69;
	public static final int Capital_F = 70;
	public static final int Capital_G = 71;
	public static final int Capital_H = 72;
	public static final int Capital_I = 73;
	public static final int Capital_J = 74;
	public static final int Capital_K = 75;
	public static final int Capital_L = 76;
	public static final int Capital_M = 77;
	public static final int Capital_N = 78;
	public static final int Capital_O = 79;
	public static final int Capital_P = 80;
	public static final int Capital_Q = 81;
	public static final int Capital_R = 82;
	public static final int Capital_S = 83;
	public static final int Capital_T = 84;
	public static final int Capital_U = 85;
	public static final int Capital_V = 86;
	public static final int Capital_W = 87;
	public static final int Capital_X = 88;
	public static final int Capital_Y = 89;
	public static final int Capital_Z = 90;
	public static final int Left_Square_Bracket = 91;
	public static final int Reverse_Solidus = 92;
	public static final int Right_Square_Bracket = 93;
	public static final int Circumflex_Accent = 94;
	public static final int Underscore = 95;
	public static final int Grave_Accent = 96;
	public static final int Small_A = 97;
	public static final int Small_B = 98;
	public static final int Small_C = 99;
	public static final int Small_D = 100;
	public static final int Small_E = 101;
	public static final int Small_F = 102;
	public static final int Small_G = 103;
	public static final int Small_H = 104;
	public static final int Small_I = 105;
	public static final int Small_J = 106;
	public static final int Small_K = 107;
	public static final int Small_L = 108;
	public static final int Small_M = 109;
	public static final int Small_N = 110;
	public static final int Small_O = 111;
	public static final int Small_P = 112;
	public static final int Small_Q = 113;
	public static final int Small_R = 114;
	public static final int Small_S = 115;
	public static final int Small_T = 116;
	public static final int Small_U = 117;
	public static final int Small_V = 118;
	public static final int Small_W = 119;
	public static final int Small_X = 120;
	public static final int Small_Y = 121;
	public static final int Small_Z = 122;
	public static final int Left_Curly_Bracket = 123;
	public static final int Vertical_Line = 124;
	public static final int Right_Curly_Bracket = 125;
	public static final int Tilde = 126;
	public static final int Delete = 127;
}
